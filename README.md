# Mini-projet : Gestionnaire de Bibliothèque

## Description :
Vous devez créer un programme simple de gestion de bibliothèque en utilisant les principes de la programmation orientée objet. Le programme doit permettre d'ajouter, supprimer et afficher des livres. Chaque livre doit avoir un titre, un auteur et un numéro d'identification unique. Le gestionnaire de bibliothèque doit également permettre d'emprunter et de retourner des livres.

## Fonctionnalités attendues :

- Ajouter un livre à la bibliothèque avec titre, auteur et numéro d'identification unique. ✅
- Supprimer un livre de la bibliothèque en utilisant son numéro d'identification. ✅
- Afficher la liste complète des livres dans la bibliothèque. ✅
- Emprunter un livre (marquer comme emprunté) en utilisant son numéro d'identification.
- Retourner un livre (marquer comme disponible) en utilisant son numéro d'identification.
- Contraintes techniques :

Utilisez au moins deux classes : une pour représenter un livre et une autre pour gérer la bibliothèque.
Utilisez les concepts d'encapsulation, d'héritage (si possible) et de polymorphisme (si possible).
Le programme doit être interactif, permettant à l'utilisateur de choisir les actions à effectuer.

## Bonus (optionnel) :

- Gérer les informations des emprunteurs (nom, date d'emprunt, date de retour prévue, etc.).
- Ajouter une fonction de recherche de livres par auteur ou titre.
- Implémenter une interface graphique simple.