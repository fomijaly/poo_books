
class Book{
    constructor(bookName, author, id){
        this.bookName = bookName;
        this.author = author;
        this.id = id;
    }
}

class Library{
    constructor(){
        this.library = [];
    }

    addBook(book){
        this.library.push(book);
    }

    deleteBook(id){
        this.library = this.library.filter(book => book.id !== id);
    }

    showBooks(){
        this.library.forEach(book => console.log(`${book.bookName}`));
    }
}

export {Library, Book};