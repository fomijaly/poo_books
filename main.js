import {Library, Book} from "./Book.js";

const bibliotheque = new Library();
const vendredi = new Book("Vendredi ou la vie sauvage", "Michel Tournier", 1);
const persuasion = new Book("Persuasion", "Jane Austen", 2);
//Ajouter un livre à la biliothèque
bibliotheque.addBook(vendredi);
bibliotheque.addBook(persuasion);

//Supprimer un livre selon son id
bibliotheque.deleteBook(2);

//Afficher les livres de la bibliothèque
bibliotheque.showBooks();